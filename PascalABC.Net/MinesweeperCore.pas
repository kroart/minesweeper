﻿unit MinesweeperCore;

interface

type OpenedCell = class
  
  private
    _x, _y, minesAround: byte;
    hasMine: boolean;
  
  public
    constructor (x: byte; y: byte; minesAround: byte; hasMine: boolean);
    begin
      _x := x;
      _y := y;
      self.minesAround := minesAround;
      self.hasMine := hasMine;
    end;
    
    property x: byte read _x;
    property y: byte read _y;
end;


type
  Minefield = class
  
  private
    _w, _h, _minesAmount, markedMines, _totalFlags: byte;
    minefield, minesAround: array[,] of byte;
    exploded: boolean := false;
    fieldCleared: boolean := false;
    _unopenedCells: byte;
    
    procedure incMinesAmount(const x, y: byte);
    begin
      if (x > -1) and (y > -1) and (x < _w) and (y < _h) then
        minesAround[x, y] := minesAround[x, y] + 1;
    end;
    
    procedure AppendOpenedCellsArray(var appendTo: array of OpenedCell; appendingArray: array of OpenedCell);
    begin
      var n := appendTo.Length;
      SetLength(appendTo, appendTo.Length + appendingArray.Length);
      for var i:=0 to appendingArray.Length - 1 do begin
        appendTo[n] := appendingArray[i];
        Inc(n);
      end;
    end;
  
  
  public
    const CELL_FLAG_HAS_MINE: byte = 1;
    const CELL_FLAG_OPENED: byte = 1 shl 1;
    const CELL_FLAG_FLAGGED: byte = 1 shl 2;
    
    property width: byte read _w;
    property height: byte read _h;
    property minesAmount: byte read _minesAmount;
    property totalFlags: byte read _totalFlags;
    property unopenedCells: byte read _unopenedCells;
    
    constructor (w: byte; h: byte; minesAmount: byte);
    begin
      _w := w;
      _h := h;
      _minesAmount := minesAmount;
      minefield := new byte[_w, _h];
      var remainingMines := minesAmount;
      while remainingMines > 0 do
      begin//расстановка мин
        var mineX := Random(w);
        var mineY := Random(h);
        var cell := minefield[mineX, mineY];
        if (cell and CELL_FLAG_HAS_MINE = 0) then begin//на клетке еще нет мины
          minefield[mineX, mineY] := cell or CELL_FLAG_HAS_MINE;
          Dec(remainingMines);
        end;
      end;
      
      //прописывание количества мин вокруг каждой клетки
      minesAround := new byte[_w, _h];
      for var j := 0 to _h - 1 do
        for var i := 0 to _w - 1 do
          if ((minefield[i, j] and CELL_FLAG_HAS_MINE) = CELL_FLAG_HAS_MINE) then begin//клетка с миной
            incMinesAmount(i - 1, j - 1);
            incMinesAmount(i, j - 1);
            incMinesAmount(i + 1, j - 1);
            incMinesAmount(i - 1, j);
            incMinesAmount(i + 1, j);
            incMinesAmount(i - 1, j + 1);
            incMinesAmount(i, j + 1);
            incMinesAmount(i + 1, j + 1);
          end;
      
      markedMines := 0;
      _totalFlags := 0;
      _unopenedCells := _w * _h;
    end;
    
    function openCell(const x, y: byte): array of OpenedCell;
    begin
      Result := new OpenedCell[0];
      
      if (x < 0) or (y < 0) or (x >= _w) or (y >= _h) then //за границами
        exit;
      var cell := minefield[x, y];
      if (cell and CELL_FLAG_OPENED) = CELL_FLAG_OPENED then //клетка уже открыта
        exit;
      if (cell and CELL_FLAG_FLAGGED) = CELL_FLAG_FLAGGED then //клетка с флагом - нельзя открывать
        exit;
      
      minefield[x, y] := (cell or CELL_FLAG_OPENED); //открыли клетку
      var hasMine := minefield[x, y] and CELL_FLAG_HAS_MINE = CELL_FLAG_HAS_MINE;
      SetLength(Result, 1);
      Result[0] := new OpenedCell(x, y, minesAround[x, y], hasMine);
      Dec(_unopenedCells);
      
      if hasMine then begin //клетка с миной - гейм овер
        exploded := true;
        for var j := 0 to _h - 1 do
          for var i := 0 to _w - 1 do
            if (minefield[i, j] and CELL_FLAG_HAS_MINE) = CELL_FLAG_HAS_MINE then //клетка с миной - открываем
              AppendOpenedCellsArray(Result, openCell(i, j));
      end
      else begin //клетка без мины
        if minesAround[x, y] = 0 then begin //открыта пустая клетка с 0 и без мины - открываем все соседние клетки
          AppendOpenedCellsArray(Result, openCell(x - 1, y - 1));
          AppendOpenedCellsArray(Result, openCell(x, y - 1));
          AppendOpenedCellsArray(Result, openCell(x + 1, y - 1));
          AppendOpenedCellsArray(Result, openCell(x - 1, y));
          AppendOpenedCellsArray(Result, openCell(x + 1, y));
          AppendOpenedCellsArray(Result, openCell(x - 1, y + 1));
          AppendOpenedCellsArray(Result, openCell(x, y + 1));
          AppendOpenedCellsArray(Result, openCell(x + 1, y + 1));
        end;
      end;
      
      if unopenedCells = _minesAmount then
        fieldCleared := true;
    end;
    
    function toggleCellFlag(const x, y: byte): boolean;//возвращает true если все мины были помечены
    begin
      var cell := minefield[x, y];
      if (cell and CELL_FLAG_OPENED) = CELL_FLAG_OPENED then //клетка уже открыта - не ставим на нее флаг
        exit;
      minefield[x, y] := cell xor CELL_FLAG_FLAGGED;
      
      var wasFlagged := minefield[x, y] and CELL_FLAG_FLAGGED = CELL_FLAG_FLAGGED; //игрок поставил флаг
      if wasFlagged then
        Inc(_totalFlags)
      else //игрок снял флаг
        Dec(_totalFlags);
      
      if minefield[x, y] and CELL_FLAG_HAS_MINE = CELL_FLAG_HAS_MINE then begin//на клетке есть мина
        if wasFlagged then //поставили флаг
          Inc(markedMines)
        else//сняли флаг
          Dec(markedMines);
      end;
      
      fieldCleared := (markedMines = _minesAmount) and (markedMines = _totalFlags);
      Result := fieldCleared;
    end;
    
    //возвращает есть ли мина в клетке. только для открытой клетки
    function cellHasMine(const x, y:byte): boolean;
    begin
      if isCellOpened(x, y) then
        Result := (minefield[x, y] and CELL_FLAG_HAS_MINE) = CELL_FLAG_HAS_MINE
      else
        Result := false;
    end;
    
    function isCellFlagged(const x, y: byte): boolean;
    begin
      if (x < 0) or (y < 0) or (x >= _w) or (y >= _h) then //за границами
        Result := false
      else
        Result := (minefield[x, y] and CELL_FLAG_FLAGGED) = CELL_FLAG_FLAGGED;
    end;
    
    function isCellOpened(const x, y: byte): boolean;
    begin
      Result := (minefield[x, y] and CELL_FLAG_OPENED) = CELL_FLAG_OPENED;
    end;
    
    function getMinesAround(const x, y: byte): byte;
    begin
      Result := minesAround[x, y];
    end;
    
    function isExploded(): boolean;
    begin
      Result := exploded;
    end;
    
    function isFieldCleared(): boolean;
    begin
      Result := fieldCleared;
    end;
  
  end;
  
  
implementation

end.