﻿//TODO считать время игры и лучшие результаты по каждой сложности
//TODO добавить звук

uses crt;
uses MinesweeperCore;

var
  pickedDifficulty, gameState: byte;
  field: Minefield;

const
  hintLine: integer = 18;

function getFieldPosByScreenPos(): array of byte;
begin
  var x := (WhereX + 1) div 3 - 1;
  var y := WhereY - 1;
  Result := new byte[2](x, y);
end;

const
  DIFFICULTY_EASY: byte = 0;
  DIFFICULTY_MEDIUM: byte = 1;
  DIFFICULTY_EXPERT: byte = 2;
  
  GAME_STATE_MAIN_MENU: byte = 0;
  GAME_STATE_READY: byte = 1;
  GAME_STATE_PLAY: byte = 2;

procedure drawMenu();
begin
  HideCursor;
  var y := 6;
  GotoXY(9, y);
  if pickedDifficulty = DIFFICULTY_EASY then
    Write('[EASY]')
  else
    Write(' EASY ');
  
  GotoXY(17, y);
  if pickedDifficulty = DIFFICULTY_MEDIUM then
    Write('[MEDIUM]')
  else
    Write(' MEDIUM ');
  
  GotoXY(27, y);
  if pickedDifficulty = DIFFICULTY_EXPERT then
    Write('[EXPERT]')
  else
    Write(' EXPERT ');
  Writeln('');
end;

procedure drawFlagsToSetAmount();
begin
  var x := WhereX;
  var y := WhereY;
  GotoXY(74, hintLine);
  WriteFormat('{0}   ', field.minesAmount - field.totalFlags);
  GotoXY(x, y);
end;

procedure drawCell(const x, y: byte);
begin
  var cursorX := (x + 1) * 3 - 2;
  var cursorY := y + 1;
  GotoXY(cursorX, cursorY);
  if field.isCellOpened(x, y) then //открыта клетка
  begin
    if field.cellHasMine(x, y) then
      write(' * ')
    else
      write(' ' + field.getMinesAround(x, y) + ' ');
  end
  else begin//закрыта клетка
    if field.isCellFlagged(x, y) then
      write('[!]')
    else
      write('[ ]');
  end;
  GotoXY(cursorX + 1, cursorY);
end;

procedure drawCells(cells: array of OpenedCell);
begin
  var curX := WhereX;
  var curY := WhereY;
  foreach var openedCell in cells do
    drawCell(openedCell.x, openedCell.y);
  GotoXY(curX, curY);
end;

procedure drawField();
begin
  ClrScr();
  for var j := 0 to field.height - 1 do
  begin
    for var i := 0 to field.width - 1 do
    begin
      drawCell(i, j);
    end;
    writeln();
  end;
end;

procedure startGame();
begin
  var w, h, mines: byte;
  case pickedDifficulty of
    DIFFICULTY_EASY:
      begin
        w := 9;
        h := 9;
        mines := 10;
      end;
    DIFFICULTY_MEDIUM:
      begin
        w := 16;
        h := 16;
        mines := 40;
      end;
    DIFFICULTY_EXPERT:
      begin
        w := 30;
        h := 16;
        mines := 99;
      end;
  end;
  
  field := new Minefield(w, h, mines);
  drawField();
  
  GotoXY(1, hintLine);
  Write('Пробел — открыть клетку');
  GotoXY(29, hintLine);
  Write('z — поставить/снять флажок');
  GotoXY(60, hintLine);
  Write('Осталось мин: ');
  drawFlagsToSetAmount;
  
  GotoXY((w div 2) * 3 - 1, h div 2);
  
  ShowCursor;
  
  gameState := GAME_STATE_READY;
end;

procedure goToMainMenu();
begin
  gameState := GAME_STATE_MAIN_MENU;
  ClrScr;
  drawMenu;
end;

procedure performVictory();
begin
  GotoXY(1, field.height + 2);
  writeln('Победа!');
  writeln('Еще раз? (y/n)');
end;

procedure handlePlayerInput();
begin
  while KeyPressed do
  begin
    var key := ReadKey;
    if (gameState = GAME_STATE_MAIN_MENU) then begin//меню
      case key of
        #0:
          begin
            key := ReadKey;
            case key of
              #37:
                begin//left
                  if pickedDifficulty = DIFFICULTY_EASY then
                    pickedDifficulty := DIFFICULTY_EXPERT
                  else
                    Dec(pickedDifficulty);
                  drawMenu();
                end;
              #39:
                begin//right
                  if pickedDifficulty = DIFFICULTY_EXPERT then
                    pickedDifficulty := DIFFICULTY_EASY
                  else
                    Inc(pickedDifficulty);
                  drawMenu();
                end;
            end
          end;
        #13: //enter
        startGame();
      end;
    end
    
    else begin //игра
      if field.isExploded or field.isFieldCleared then begin //игра закончена
        case key of
          'y', 'Y':
            begin//y
              startGame();
            end;
          'n', 'N':
          goToMainMenu();
        end;
      end
      else begin
          case key of
          #0:
            begin
              key := ReadKey;
              if not field.isExploded then //игра
                case key of
                  #37: GotoXY(WhereX - 3, whereY); //left
                  #38: GotoXY(WhereX, whereY - 1); //up
                  #39: GotoXY(WhereX + 3, whereY); //right
                  #40: GotoXY(WhereX, whereY + 1); //down
                end;
            end;
          #32:
            begin//пробел - открываем клетку
              var pos := GetFieldPosByScreenPos();
              var cellX := pos[0];
              var cellY := pos[1];
              if (field.isCellOpened(cellX, cellY)) then begin //клетка открыта - смотрим можно ли открывать все соседние
                var flaggenMinesAround := 0;
                for var dj := -1 to 1 do
                  for var di := -1 to 1 do
                    if (field.isCellFlagged(cellX + di, cellY + dj)) then
                      Inc(flaggenMinesAround);
                if (flaggenMinesAround = field.getMinesAround(cellX, cellY)) then begin //вокруг все помечено - можно открывать остальные клетки
                  for var dj := -1 to 1 do
                    for var di := -1 to 1 do
                      drawCells(field.openCell(cellX + di, cellY + dj));
                end;
              end
              else if (not field.isCellFlagged(cellX, cellY)) then begin //клетка без флага - открываем
              
                if gameState = GAME_STATE_READY then begin //
                  while field.cellHasMine(cellX, cellY) do
                    field := new Minefield(field.width, field.height, field.minesAmount);
                  gameState := GAME_STATE_PLAY;
                end;
                
                drawCells(field.openCell(cellX, cellY));
                
                if field.cellHasMine(cellX, cellY) then begin //открыли клетку с миной - Game Over
                  GotoXY(1, field.height + 4);
                  writeln('Взорвался!');
                  write('Еще раз? (y/n)');
                end
              end;
              
              if field.unopenedCells = field.minesAmount then //победа
                  performVictory;
            end;
          #122:
            begin//z - toggle flag
              var pos := GetFieldPosByScreenPos();
              var won := field.toggleCellFlag(pos[0], pos[1]);
              drawCell(pos[0], pos[1]);
              drawFlagsToSetAmount;
              if won then begin//игрок выиграл
                performVictory();
              end;
            end;
          #27:
            begin//esc
              goToMainMenu;
            end;
        end;
      end;
    end;
    
  end;
end;

begin
  var frameTime := Round(1000 / 30);
  
  pickedDifficulty := DIFFICULTY_MEDIUM;
  
  gameState := GAME_STATE_MAIN_MENU;
  drawMenu();
  
  while (True) do
  begin
    handlePlayerInput();
    
    Sleep(frameTime);
  end;
end.
