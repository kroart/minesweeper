﻿///Реализация только начата. Еще ничего не работает
//TODO срабатывание клика кнопки при отпускании кнопки, а не при нажимании

uses WPFObjects;

type
  Button = class(RoundRectWPF)
    private
    static btns: set of Button := [];
    
    public
    Click: procedure;
    
    constructor (x, y, w,h, r: real; c: GColor; borderWidth: real; borderColor: GColor; text: string; ClickAction: procedure);
    begin
      inherited Create(x, y, w, h, r, c, borderWidth, borderColor);
      Self.Text := text;
      Click := ClickAction;
      Include(btns, Self);
    end;
  end;
  
procedure StartGame();
begin
  writeln('Start game');
end;

begin
  OnMouseDown := (x, y: real; mousebutton: integer) -> begin
    if mousebutton = 2 then
      exit;
    foreach var btn in Button.btns do begin
      if (x > btn.Left) and (x < btn.Right) and (y > btn.Top) and (y < btn.Bottom) then begin
        (btn as Button).Click();
        exit;
      end;
    end;
  end;
  
  var btnX := 20;
  var btnY := 20;
  var btnGap := 20;
  var buttonEasy : Button := new Button(btnX, btnY, 300, 50, 5, GColor.FromRgb(128, 128, 128), 0, GColor.FromRgb(0, 0, 0), 'Easy', () -> begin
      writeln('Easy');
      StartGame();
    end);
  btnY := buttonEasy.Bottom.Round() + btnGap;
  var buttonMedium : Button := new Button(btnX, btnY, 300, 50, 5, GColor.FromRgb(128, 128, 128), 0, GColor.FromRgb(0, 0, 0), 'Medium', () -> begin
      writeln('Medium');
      StartGame();
    end);
  btnY := buttonMedium.Bottom.Round() + btnGap;
  var buttonExpert : Button := new Button(btnX, btnY, 300, 50, 5, GColor.FromRgb(128, 128, 128), 0, GColor.FromRgb(0, 0, 0), 'Expert', () -> begin
      writeln('Expert');
      StartGame();
    end);
  
  
end.
